
## osx notes

 * meta key
    Meta key is set to left alt on my mbp/iterm2

## Pane layout

 * Switch panes to vertical even
  
    c-b m-1

 * Switch panes to horizontal even
  
    c-b m-2