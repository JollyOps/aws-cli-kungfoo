#!/bin/bash

# Avoid having to issue --no-cli-pager
export AWS_PAGER=""

# Create
aws sqs create-queue \
    --region eu-west-1 \
    --queue-name FooQ \
    --attributes '{
        "MessageRetentionPeriod": "3600",
        "VisibilityTimeout": "60"
    }' \
    --tags DeleteMe=True


# Get Q URL
QURL=$(
    aws sqs get-queue-url \
        --queue-name FooQ \
        --query QueueUrl \
        --output text
)
echo $QURL

# Send msg into Q
aws sqs send-message \
    --queue-url $QURL \
    --message-body '{
        "k1":"v1",
        "k2":"v2",
        "k3":"v3"
    }'

# Recieve msg from Q (with longpoll)
aws sqs receive-message \
    --queue-url $QURL --wait-time-seconds 10 | jq

# List sqs with name prefix
aws sqs list-queues \
    --queue-name-prefix Foo


# Delete Q by URL
aws sqs delete-queue \
    --query-url $QURL



#### Extra Tricks

# Delete Msg from queue
MSG=$(aws sqs receive-message --queue-url $QURL)
# Looks like if you want your shell to not interpret backslashes 
# (i.e., turn them into newlines or whatever), you need to use printf
# instead of echo:
RCHANDLE=$(printf "%s" $MSG | jq -r '.Messages[0].ReceiptHandle')

aws sqs delete-message --queue $QURL --receipt-handle $RCHANDLE