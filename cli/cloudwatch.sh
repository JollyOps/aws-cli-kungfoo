#!/bin/bash

export AWS_PAGER=""

# Some mobile for SMS notification endpoint of SNS topic subscriber
PHONE_NUM=+4487515123456

# Listing Metrics
aws cloudwatch list-metrics \
    --namespace AWS/SQS \
    --metric-name ApproximateNumberOfMessagesVisible \
    --dimensions '[{"Name":"QueueName","Value":"FooQ"}]'

# Above is equiv to below
aws cloudwatch list-metrics \
    --dimensions '[
        {"Name":"Namespace",  "Value":"AWS/SQS"},
        {"Name":"QueueName",  "Value":"FooQ"},
        {"Name":"MetricName", "Value":"ApproximateNumberOfMessagesVisible"}
    ]'

# Create Topic and remember ARN
TOPIC_ARN=$(
     aws sns create-topic \
        --name DeepFooQueueAlert \
        --tags Key=DeleteMe,Value=True \
        --query "TopicArn" \
        --output text
)

# Subscriber mobile phone to Topic
aws sns subscribe \
    --topic-arn $TOPIC_ARN \
    --protocol sms \
    --notification-endpoint $PHONE_NUM


# Create alarm to watch Length of SQS Queue
aws cloudwatch put-metric-alarm \
    --alarm-name DeepFooQ \
    --alarm-description "Alarm when Q is congested" \
    --namespace AWS/SQS \
    --metric-name ApproximateNumberOfMessagesVisible \
    --dimensions '[
        {"Name":"QueueName",  "Value":"FooQ"}
    ]' \
    --statistic Average \
    --period 60 \
    --evaluation-periods 3 \
    --datapoints-to-alarm 2 \
    --treat-missing-data notBreaching \
    --threshold 10 \
    --comparison-operator GreaterThanThreshold  \
    --alarm-actions $TOPIC_ARN


